use epc_qrcode::EpcQRCode;
use iban::Iban;
use image::Luma;
use std::str::FromStr;

pub fn main() {
    let code = EpcQRCode::new_minimal_with_usage(
        Iban::from_str("AT52 3506 3000 1801 1767").unwrap(),
        "Evangelische Jugend Burg Finstergruen",
        105f32,
        None,
        "Burgfest Felix Resch",
    )
    .into_qrcode()
    .unwrap();

    let image = code.render::<Luma<u8>>().build();

    image.save("code.png").unwrap();
}
