use iso3166_1::{alpha2, CountryCode};
use std::fmt::{Display, Formatter};

#[derive(Debug)]
pub struct Bic<'a> {
    bank_code: &'a str,
    country_code: CountryCode<'a>,
    location_code: &'a str,
    branch_code: Option<&'a str>,
}

impl PartialEq for Bic<'_> {
    fn eq(&self, other: &Self) -> bool {
        self.bank_code == other.bank_code &&
            self.country_code.num == other.country_code.num &&
            self.location_code == other.location_code &&
            self.branch_code == other.branch_code
    }
}

#[cfg(test)]
mod test {
    use crate::bic::{Bic, ParseBicError};

    #[test]
    fn test_parse_valid_bic() {
        let bic = Bic::parse("BKAUATWW");

        if let Ok(bic) = bic {
            assert_eq!("BKAU", bic.bank_code);
            assert_eq!(iso3166_1::alpha2("AT").unwrap().num, bic.country_code.num);
            assert_eq!("WW", bic.location_code);
            assert_eq!(None, bic.branch_code);
        } else {
            assert!(false);
        }

        let bic = Bic::parse("GIBAATWWXXX");

        if let Ok(bic) = bic {
            assert_eq!("GIBA", bic.bank_code);
            assert_eq!(iso3166_1::alpha2("AT").unwrap().num, bic.country_code.num);
            assert_eq!("WW", bic.location_code);
            assert_eq!(Some("XXX"), bic.branch_code);
        } else {
            assert!(false);
        }
    }

    #[test]
    fn test_invalid_length_too_short_8() {
        let bic = Bic::parse("BKAUATW");

        assert_eq!(Err(ParseBicError::InvalidLength), bic);
    }

    #[test]
    fn test_invalid_length_too_long_8() {
        let bic = Bic::parse("BKAUATWWW");

        assert_eq!(Err(ParseBicError::InvalidLength), bic);
    }

    #[test]
    fn test_invalid_length_too_short_11() {
        let bic = Bic::parse("GIBAATWWXX");

        assert_eq!(Err(ParseBicError::InvalidLength), bic);
    }

    #[test]
    fn test_invalid_length_too_long_11() {
        let bic = Bic::parse("GIBAATWWXXXX");

        assert_eq!(Err(ParseBicError::InvalidLength), bic);
    }

    #[test]
    fn test_invalid_country_code() {
        let bic = Bic::parse("GIBAADWWXXX");

        assert_eq!(Err(ParseBicError::UnknownCountryCode("AD")), bic);
    }
}

#[derive(Debug, Eq, PartialEq)]
pub enum ParseBicError<'a> {
    InvalidLength,
    UnknownCountryCode(&'a str),
}

impl Display for ParseBicError<'_> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            ParseBicError::InvalidLength => {
                write!(f, "bic has invalid length")
            }
            ParseBicError::UnknownCountryCode(country_code) => {
                write!(f, "unknown country code {}", country_code)
            }
        }
    }
}


impl<'a> Bic<'a> {
    pub fn parse(s: &'a str) -> Result<Self, ParseBicError<'a>> {
        if s.len() != 8 && s.len() != 11 {
            return Err(ParseBicError::InvalidLength);
        }
        let bank_code = &s[0..4];
        let country_code = &s[4..6];
        let location_code = &s[6..8];
        let country_code = match alpha2(country_code) {
            Some(country_code) => country_code,
            None => return Err(ParseBicError::UnknownCountryCode(country_code)),
        };
        let branch_code = if s.len() == 11 { Some(&s[8..11]) } else { None };
        Ok(Bic::<'a> {
            bank_code,
            country_code,
            location_code,
            branch_code,
        })
    }
}

impl Display for Bic<'_> {

    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.bank_code).unwrap();
        write!(f, "{}", self.country_code.alpha2).unwrap();
        write!(f, "{}", self.location_code).unwrap();
        if let Some(branch_code) = self.branch_code {
            write!(f, "{}", branch_code).unwrap();
        }
        Ok(())
    }
}
