use iban::IbanLike;

pub mod bic;

use crate::bic::Bic;
pub use qrcode;
use qrcode::{EcLevel, QrCode, QrResult};
use std::fmt::Write;

pub struct EpcQRCode<'bic, 'rec, 'r, 're, 'us, 'hint, I>
where
    I: IbanLike,
{
    version: EpcVersion,
    iban: I,
    bic: Option<Bic<'bic>>,
    recipient: &'rec str,
    amount: f32,
    reason: Option<&'r str>,
    reference: Option<&'re str>,
    usage: Option<&'us str>,
    user_hints: Option<&'hint str>,
}

impl<I> EpcQRCode<'_, '_, '_, '_, '_, '_, I>
where
    I: IbanLike,
{
    pub fn new_minimal_with_usage<'rec, 'r, 'us>(
        iban: I,
        recipient: &'rec str,
        amount: f32,
        reason: Option<&'r str>,
        usage: &'us str,
    ) -> EpcQRCode<'static, 'rec, 'r, 'static, 'us, 'static, I> {
        EpcQRCode {
            iban,
            version: EpcVersion::Version2,
            bic: None,
            recipient,
            amount,
            reason,
            reference: None,
            usage: Some(usage),
            user_hints: None,
        }
    }

    pub fn new_with_usage_and_bic<'bic, 'rec, 'r, 'us>(
        iban: I,
        bic: Bic<'bic>,
        recipient: &'rec str,
        amount: f32,
        reason: Option<&'r str>,
        usage: &'us str,
    ) -> EpcQRCode<'bic, 'rec, 'r, 'static, 'us, 'static, I> {
        EpcQRCode {
            iban,
            version: EpcVersion::Version2,
            bic: Some(bic),
            recipient,
            amount,
            reason,
            reference: None,
            usage: Some(usage),
            user_hints: None,
        }
    }

    pub fn into_qrcode(self) -> QrResult<QrCode> {
        let EpcQRCode {
            iban,
            version,
            bic,
            recipient,
            amount,
            reason,
            reference,
            usage,
            user_hints,
        } = self;
        let mut buffer = String::with_capacity(50);
        writeln!(buffer, "BCD").unwrap();
        writeln!(buffer, "{:>03}", version as u32).unwrap();
        writeln!(buffer, "2").unwrap();
        writeln!(buffer, "SCT").unwrap();
        if let Some(bic) = bic {
            writeln!(buffer, "{}", bic.to_string()).unwrap();
        } else {
            writeln!(buffer).unwrap();
        }
        writeln!(buffer, "{}", recipient).unwrap();
        writeln!(buffer, "{}", iban.electronic_str()).unwrap();
        writeln!(buffer, "EUR{:0.2}", amount).unwrap();
        if let Some(reason) = reason {
            writeln!(buffer, "{}", reason).unwrap();
        } else {
            writeln!(buffer).unwrap();
        }
        if let Some(reference) = reference {
            writeln!(buffer, "{}", reference).unwrap();
        } else {
            writeln!(buffer).unwrap();
        }
        if let Some(usage) = usage {
            writeln!(buffer, "{}", usage).unwrap()
        } else {
            writeln!(buffer).unwrap()
        }
        if let Some(user_hints) = user_hints {
            writeln!(buffer, "{}", user_hints).unwrap()
        } else {
            writeln!(buffer).unwrap()
        }
        qrcode::QrCode::with_error_correction_level(buffer, EcLevel::M)
    }
}

#[repr(u32)]
pub enum EpcVersion {
    Version1 = 1,
    Version2 = 2,
}
